from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Account


def signup(request):
# Define a function called signup for a given server request.
    if request.method == "POST":
    # If the method of the request is Post
        form = UserCreationForm(request.POST)
        # Set a variable form to the stock form UCF which has a post method for receiving input info from the user
        if form.is_valid():
        # Checks if the form is valid
            username = request.POST.get("username")
            # make a request to post to the form what is pulled from the username field in the form
            password = request.POST.get("password1")
            # make a request to post to the form what is pulled from the password1 field in the form
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            # Sets user variable to the model which we imported at the top User, and creates in the database??? Fix this note later!!!
            user.save()
            # Saves the objects stored in the user var to the database
            login(request, user)
            # Logs the new user from the user variable in
            return redirect("home")
            # returns a redirect to the home page in the "home" path
    else:
    # If not a post request
        form = UserCreationForm()
        # sets form variable to the imported UserCreationForm
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "accountcreate.html"
    fields = ["name", "number"]
    login_url = "/accounts/login/"

    def form_valid(self,form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")
