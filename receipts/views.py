from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receiptlist.html"
    paginate_by = 10

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receiptcreate.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    login_url = "/accounts/login/"
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expensecategorylist.html"
    paginate_by = 20

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accountlist.html"
    paginate_by = 20

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expensecategorycreate.html"
    fields = ["name"]
    login_url = "/accounts/login/"
    success_url = reverse_lazy("list_categories")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")
